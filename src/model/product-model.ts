export enum CardType{
    ProductCard,
    FavoriteCard,
    CartCard
}

export class ProductModel{
    pID: string;
    pName: string;
    pDescription: string;
    isFavorite: boolean;
}