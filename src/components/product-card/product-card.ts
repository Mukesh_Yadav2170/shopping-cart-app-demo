import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { ProductModel } from '../../model/product-model';

/**
 * Imports service, constants and model
 */
import { SharedService } from '../../providers/shared-service';
import { ToastService } from '../../providers/toast-service';
import { CommonStringConstant } from '../../shared/common-string';
import { EventConstant } from '../../shared/event-constant';

@Component({
  selector: 'product-card',
  templateUrl: 'product-card.html'
})

export class ProductCard implements OnInit {

  @Input() product: ProductModel;
  @Input() pageType: any = 'ProductsPage'; //Add default when you are not giving any pageType
  @Output() addProductToFav: EventEmitter<any> = new EventEmitter();

  isFavorit: boolean = false;

  constructor(private sharedService: SharedService,
    private toast: ToastService,
    private events: Events) { }

  ngOnInit() {}

  /**
   * Add product to favorite list using event emmiter
   */
  addToFavouriteList() {
    this.isFavorit = !this.isFavorit;
    this.addProductToFav.emit(this.product);
  }

  /**
   * Add product to cart list
   *  As Example we call this service from here for modularity of code and code seperation.
   * From here we can also emit event and handle on cart page
   */
  addToCartList() {
    this.sharedService.addProductToCartList(this.product);
    this.events.publish(EventConstant.UPDATE_CART_BADGE, 1);
  }

  /**
   * Delete from cart
   * As Example we call this service from here for modularity of code and code seperation.
   * From here we can also emit event and handle on cart page
   */
  deleteFromCartList() {
    this.sharedService.removeProductFromCartList(this.product);
    this.events.publish(EventConstant.UPDATE_CART_BADGE, -1);
  }
}