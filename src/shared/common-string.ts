export class CommonStringConstant {

    /**
     * Tabs title string const
     */
    public static TAB_TITLE_PRODUCTS = 'Products';
    public static TAB_TITLE_FAVOURITE = 'Favourite';
    public static TAB_TITLE_CART = 'Cart';

}