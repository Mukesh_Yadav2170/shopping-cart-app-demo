import { Injectable } from '@angular/core';

/**
 * Imports model and service
 */
import { ProductModel } from '../model/product-model';

@Injectable()
export class SharedService {

  private favoriteProduct: ProductModel[];
  private cartProduct: ProductModel[];

  constructor() {
    this.favoriteProduct = [];
    this.cartProduct = [];
  }

  /**
   * Add favorite product to favorite list
   */
  addProductToFavoriteList(product: ProductModel) {
    this.favoriteProduct.push(product);
    console.log(this.favoriteProduct);
  }

  /**
   * Remove product from favorite list
   */
  removeProductFromFavoriteList(product) {
    const deleteObjectIndex = this.favoriteProduct.findIndex((obj) => {
      return product.pID === obj.pID;
    });
    this.favoriteProduct.splice(deleteObjectIndex, 1);
  }
  /**
   * Get favorite list 
   */
  getFavoriteList() {
    return this.favoriteProduct;
  }

  /**
   * Add product to cart list
   */
  addProductToCartList(product) {
    this.cartProduct.push(product);
  }

  /**
   * Remove product from favorite list
   */
  removeProductFromCartList(product) {
    const deleteObjectIndex = this.cartProduct.findIndex((obj) => {
      return product.pID === obj.pID;
    });
    this.cartProduct.splice(deleteObjectIndex, 1);
  }

  /**
   * Get cart list
   */
  getCartList() {
    return this.cartProduct;
  }
}
