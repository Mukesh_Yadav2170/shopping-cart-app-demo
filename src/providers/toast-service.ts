import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class ToastService {

  constructor(private toastCtrl: ToastController) { }

  /**
   * Present toast message 
   * @param message Message which you want to show
   */
  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message
    });
    toast.present();
  }

}
