import { Injectable } from '@angular/core';

import { ProductModel } from '../model/product-model';
import { PRODUCTS } from '../mockdata/product-mock-data';

@Injectable()
export class ProductService {

  constructor() { }

  /**
   * Return product list
   */
  getProductList(): ProductModel[] {
    return PRODUCTS;
  }

}
