import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

/**
 * Imports Pages
 */
import { ProductsPage } from '../pages/products-page/products-page';
import { FavouritePage } from '../pages/favourite-page/favourite-page';
import { CartPage } from '../pages/cart-page/cart-page';
import { TabsPage } from '../pages/tabs/tabs';

/**
 * Imports Components
 */
import { ProductCard } from '../components/product-card/product-card';

/**
 * Imports Providers
 */
import { ProductService } from '../providers/product-service';
import { SharedService } from '../providers/shared-service';
import { ToastService } from '../providers/toast-service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    ProductsPage,
    FavouritePage,
    CartPage,
    TabsPage,
    ProductCard,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProductsPage,
    FavouritePage,
    CartPage,
    TabsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ProductService,
    SharedService,
    ToastService,
  ]
})
export class AppModule { }
