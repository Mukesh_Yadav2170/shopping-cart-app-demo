import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Imports model, providers, constant file
 */
import { ProductModel } from '../../model/product-model';
import { ProductService } from '../../providers/product-service';
import { ToastService } from '../../providers/toast-service';
import { SharedService } from '../../providers/shared-service';
import { CommonStringConstant } from '../../shared/common-string';

@Component({
  selector: 'page-products-page',
  templateUrl: 'products-page.html',
})
export class ProductsPage implements OnInit {

  productList: ProductModel[];
  favouriteCount: number = 0;
  productTitle: string;
  public pageType: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private productService: ProductService,
    private sharedService: SharedService,
    private toast: ToastService) {
    this.productList = [];
  }

  ngOnInit() {
    this.viewInitialization();
    this.productList = this.productService.getProductList();
  }

  /**
   * Initialize view
   */
  viewInitialization() {
    this.productTitle = CommonStringConstant.TAB_TITLE_PRODUCTS;
    this.pageType = this.navCtrl.getActive().component.name;
  }

  /**
   * Add product to favorite list
   * @param product product object
   */
  addProductToFavoriteList(product) {
    if (product.isFavorite) {
      product.isFavorite = false;
      this.favouriteCount -= 1;
      this.sharedService.removeProductFromFavoriteList(product);
    } else {
      product.isFavorite = true;
      this.favouriteCount += 1;
      this.sharedService.addProductToFavoriteList(product);
    }
  }
}
