import { Component, OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
/**
 * Imports Pages
 */
import { ProductsPage } from '../products-page/products-page';
import { FavouritePage } from '../favourite-page/favourite-page';
import { CartPage } from '../cart-page/cart-page';

/**
 * Imports constant file and providers
 */
import { CommonStringConstant } from '../../shared/common-string';
import { EventConstant } from '../../shared/event-constant';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage implements OnInit {

  productTabTitle: string;
  favouriteTabTitle: string;
  cartTabTitle: string;
  private noOfProduct: number = 0;

  tabProduct = ProductsPage;
  tabFavourity = FavouritePage;
  tabCart = CartPage;

  constructor(private events: Events) {
    this.subscribeForEvents();
  }

  ngOnInit() {
    this.viewInitialization();
  }

  ionViewWillLeav(){
    this.unSubscribeForEvents();
  }

  /**
   * Initialize View
   */
  viewInitialization() {
    this.productTabTitle = CommonStringConstant.TAB_TITLE_PRODUCTS;
    this.favouriteTabTitle = CommonStringConstant.TAB_TITLE_FAVOURITE;
    this.cartTabTitle = CommonStringConstant.TAB_TITLE_CART;
  }

/**
 * Subscribe event
 */
  subscribeForEvents(){
    this.events.subscribe(EventConstant.UPDATE_CART_BADGE, (cartCount) => {
      this.noOfProduct += cartCount;
    });
  }

  /**
   * unSubscribeForEvents
   */
  unSubscribeForEvents() {
    this.events.unsubscribe(EventConstant.UPDATE_CART_BADGE);
  }
}
