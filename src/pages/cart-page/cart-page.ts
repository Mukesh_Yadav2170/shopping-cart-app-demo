import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs } from 'ionic-angular';

/**
 * Imports model, service and constants
 */
import { ProductModel } from '../../model/product-model';
import { SharedService } from '../../providers/shared-service';
import { CommonStringConstant } from '../../shared/common-string';

@Component({
  selector: 'page-cart-page',
  templateUrl: 'cart-page.html',
})
export class CartPage implements OnInit {

  cartList: ProductModel[];
  cartTitle: string;
  public pageType: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private sharedService: SharedService,
    private tabs: Tabs) {
    this.cartList = [];
  }

  ngOnInit() {
    this.cartTitle = CommonStringConstant.TAB_TITLE_CART;
  }

  /**
   * Call when view will enter
   */
  ionViewWillEnter() {
    this.cartList = this.sharedService.getCartList();
    this.pageType = this.navCtrl.getActive().component.name;
  }

  /**
   * Open product tab
   */
  openProductTab() {
    this.tabs.select(0);
  }
}
