import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs } from 'ionic-angular';

/**
 * Imports model, service and constant file
 */
import { ProductModel } from '../../model/product-model';
import { SharedService } from '../../providers/shared-service';
import { CommonStringConstant } from '../../shared/common-string';

@Component({
  selector: 'page-favourite-page',
  templateUrl: 'favourite-page.html',
})
export class FavouritePage implements OnInit {

  favoriteList: ProductModel[];
  favouriteTitle: string;
  public pageType: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private sharedService: SharedService,
    private tabs: Tabs) {
    this.favoriteList = [];
  }

  ngOnInit() {
    this.viewInitialization();
  }

  /**
   * Call when view will enter
   */
  ionViewWillEnter() {
    this.favoriteList = this.sharedService.getFavoriteList();
    this.pageType = this.navCtrl.getActive().component.name;
  }

  /**
   * Initialize view
   */
  viewInitialization() {
    this.favouriteTitle = CommonStringConstant.TAB_TITLE_FAVOURITE;
  }

  /**
   * Open product tab
   */
  openProductTab() {
    this.tabs.select(0);
  }
}
